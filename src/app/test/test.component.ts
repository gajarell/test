import { Component, OnInit, AfterViewChecked } from '@angular/core';
import $ from "jquery";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  test: String;

  constructor() { }

  ngOnInit() {
  }

  a() {
    $("#gg").val("test");
  }

  b() {
    //do whetever
    console.log("res:",this.test);
  }
}
